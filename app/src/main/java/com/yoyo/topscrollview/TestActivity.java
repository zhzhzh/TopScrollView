package com.yoyo.topscrollview;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.os.Bundle;

import com.yoyo.topscrollview.centerview.WaveView;

public class TestActivity extends AppCompatActivity {

    @BindView(R.id.testWaveView)
    WaveView mTestWaveView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        ButterKnife.bind(this);

    }
}
