package com.yoyo.topscrollview.common;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.SparseArray;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * author:yoyo
 * date:2020/6/12
 */
public class CommonViewHolder extends RecyclerView.ViewHolder {

    private SparseArray<View> mViews;


    public CommonViewHolder(View itemView) {
        super(itemView);
        mViews = new SparseArray<>();
    }


    public CommonViewHolder setShow(int vid) {
        View view = getView(vid);
        view.setVisibility(View.VISIBLE);
        return this;
    }

    public CommonViewHolder setGone(int vid) {
        View view = getView(vid);
        view.setVisibility(View.GONE);
        return this;
    }


    public CommonViewHolder setViewBg(int vid, int color) {
        CardView view = (CardView) getView(vid);
        view.setCardBackgroundColor(color);
        return this;
    }

    /**
     * @param vid
     * @param value
     * @return
     */
    public CommonViewHolder setText(int vid, String value) {
        TextView view = (TextView) getView(vid);
        view.setText(value);
        return this;
    }


    public CommonViewHolder setTextColor(int vid, int color) {
        TextView view = (TextView) getView(vid);
        view.setTextColor(color);
        return this;
    }


    /**
     * @param vid
     * @param onClick
     * @return
     */
    public CommonViewHolder setOnClick(int vid, View.OnClickListener onClick) {
        View view = getView(vid);
        view.setOnClickListener(onClick);
        return this;
    }

    /**
     * @param vid
     * @param onCheckedChangeListener
     * @return
     */
    public CommonViewHolder setOnCheckedChange(int vid, CompoundButton.OnCheckedChangeListener onCheckedChangeListener) {
        CheckBox view = (CheckBox) getView(vid);
        view.setOnCheckedChangeListener(onCheckedChangeListener);
        return this;
    }

    /**
     * @param vid
     * @param listener
     * @return
     */
    public CommonViewHolder setLongClick(int vid, View.OnLongClickListener listener) {
        View view = getView(vid);
        view.setOnLongClickListener(listener);
        return this;
    }

    /**
     * @param vid
     * @param resId
     * @return
     */
    public CommonViewHolder setImgResource(int vid, int resId) {
        ImageView view = (ImageView) getView(vid);
        view.setImageResource(resId);
        return this;
    }

    /**
     * @param vid
     * @param bitmap
     * @return
     */
    public CommonViewHolder setImageBitmap(int vid, Bitmap bitmap) {
        ImageView view = (ImageView) getView(vid);
        view.setImageBitmap(bitmap);
        return this;
    }


    /**
     * @param vid
     * @param color
     * @return
     */
    public CommonViewHolder setImageBackgroundColor(int vid, int color) {
        ImageView view = (ImageView) getView(vid);
        view.setBackgroundColor(color);
        return this;
    }

    public CommonViewHolder setCheckbox(int vid) {
        CheckBox checkBox = (CheckBox) getView(vid);
        if (checkBox.isChecked())
            checkBox.setChecked(false);
        Drawable[] personDraws = checkBox.getCompoundDrawables();
        personDraws[0].setBounds(0, 0, 80, 80);
        checkBox.setCompoundDrawables(personDraws[0], personDraws[1], personDraws[2], personDraws[3]);
        return this;
    }

    public CommonViewHolder setRvAdapter(int vid, CommonAdapter commonAdapter, Context context) {
        RecyclerView recyclerView = (RecyclerView) getView(vid);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        CommonAdapter adapter = commonAdapter;
        recyclerView.setAdapter(adapter);
        return this;
    }

    /**
     * @param vid
     * @return
     */
    public View getView(int vid) {
        View view = mViews.get(vid);
        if (view == null) {
            view = itemView.findViewById(vid);
            mViews.put(vid, view);
        }
        return view;
    }

}
