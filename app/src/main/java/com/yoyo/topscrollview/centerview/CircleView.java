package com.yoyo.topscrollview.centerview;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.LinearInterpolator;

import com.yoyo.topscrollview.R;

/**
 * author:yoyo
 * date:2020/6/10
 */
public class CircleView extends View {

    // 圆和圆环的画笔
    private Paint mCirclePaint;
    private Paint mRingPaint;
    private Paint mTextPaint;
    // 圆和圆环的颜色
    private int mCircleColor;
    private int mRingColor;
    // 圆和圆环的半径
    private float mStartCircleRadius;
    private float mBigCircleRadius;
    private float mRingRadius;
    // 初始圆环的所扫过的角度
    private float mRingSweepAngle = 180;

    private int mViewWidth;
    private int mViewHeight;

    private float tempRadius = 0;
    private ValueAnimator mRingAnimator;
    private Bitmap mCenterImg, mTempCenterImg;
    private float mStartCenterImgWidth, mStartCenterImgHeight;
    private float mCenterImgWidth, mCenterImgHeight;
    private boolean mCenterImgIsVisible = true;

    // 圆中间倒计时的文字
    private boolean mCenterTextIsVisible = false;
    private int mCenterText = 60;
    private float mTextSize = 135;

    // alarm状态所需的一些属性
    private boolean isAlarm = false;
    //    private Bitmap mAlarmTop,mTempAlarmTop,mAlarmBottom,mTempAlarmBottom;
    private float mStartAlarmTopWidth, mStartAlarmTopHeight;
    private float mStartAlarmBottomWidth, mStartAlarmBottomHeight;
    private float mAlarmTopWidth, mAlarmTopHeight;
    private float mAlarmBottomWidth, mAlarmBottomHeight;
    // 上面闪闪闪的透明度
    private int mAlpha = 1;
    // 闪闪闪的动画
    private ValueAnimator mAlarmAnimator;

    public CircleView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.CircleView);
        mRingColor = typedArray.getColor(R.styleable.CircleView_ring_color, Color.RED);
        mCircleColor = typedArray.getColor(R.styleable.CircleView_circle_color, Color.BLUE);

//        mAlarmTop = BitmapFactory.decodeResource(context.getResources(),  R.drawable.alarm_top);
//        mAlarmBottom = BitmapFactory.decodeResource(context.getResources(),  R.drawable.alarm_bottom);


        mCenterImg = BitmapFactory.decodeResource(context.getResources(), typedArray.getResourceId(R.styleable.CircleView_center_src, R.drawable.fish));
        mTempCenterImg = BitmapFactory.decodeResource(context.getResources(), typedArray.getResourceId(R.styleable.CircleView_center_src, R.drawable.fish));

        mCirclePaint = new Paint();
        mCirclePaint.setAntiAlias(true);
        mCirclePaint.setColor(mCircleColor);
        mCirclePaint.setStyle(Paint.Style.FILL_AND_STROKE);

        mRingPaint = new Paint();
        mRingPaint.setAntiAlias(true);
        mRingPaint.setColor(mRingColor);
        mRingPaint.setStyle(Paint.Style.STROKE);


        mCenterImgWidth = mCenterImg.getWidth();
        mCenterImgHeight = mCenterImg.getHeight();
        mStartCenterImgWidth = mCenterImg.getWidth();
        mStartCenterImgHeight = mCenterImg.getHeight();

//        mAlarmTopWidth = mAlarmTop.getWidth();
//        mAlarmTopHeight = mAlarmTop.getHeight();
//        mAlarmBottomWidth = mAlarmBottom.getWidth();
//        mAlarmBottomHeight = mAlarmBottom.getHeight();
//        mStartAlarmTopWidth = mAlarmTop.getWidth();
//        mStartAlarmTopHeight = mAlarmTop.getHeight();
//        mStartAlarmBottomWidth = mAlarmBottom.getWidth();
//        mStartAlarmBottomHeight = mAlarmBottom.getHeight();

        // 倒计时文字样式
        mTextPaint = new Paint();
        mTextPaint.setStyle(Paint.Style.FILL);
        mTextPaint.setStrokeWidth(8);
        mTextPaint.setTextSize(mTextSize);
        mTextPaint.setAntiAlias(true);
        mTextPaint.setColor(getResources().getColor(R.color.blue));
        mTextPaint.setTextAlign(Paint.Align.CENTER);
    }

    /**
     * 设置圆中间的图片
     *
     * @param context
     * @param imgId
     */
    public void setCenterImg(Context context, int imgId) {
        mCenterImg = BitmapFactory.decodeResource(context.getResources(), imgId);
        mTempCenterImg = BitmapFactory.decodeResource(context.getResources(), imgId);
        postInvalidate();
    }

    public void setCenterImgVisible(boolean isVisible) {
        mCenterImgIsVisible = isVisible;
    }


    /**
     * 设置圆中间倒计时的文字
     *
     * @param context
     * @param text
     */
    public void setCenterText(Context context, int text) {
        mCenterText = text;
        postInvalidate();
    }

    /**
     * 是否显示圆中间倒计时文字
     *
     * @param isVisible
     */
    public void setCenterTextVisible(boolean isVisible) {
        mCenterTextIsVisible = isVisible;
    }


    /**
     * 设置圆的颜色
     *
     * @param color
     */
    public void setCircleColor(int color) {
        mCirclePaint.setColor(color);
        postInvalidate();
    }

    /**
     * 设置圆环的颜色
     *
     * @param color
     */
    public void setRingColor(int color) {
        mRingPaint.setColor(color);
        postInvalidate();
    }

    /**
     * 设置圆和圆环的半径
     * 圆环是设置画笔的宽度
     *
     * @param circleRadius
     */
    public void setCircleRadius(float circleRadius) {
        mBigCircleRadius = circleRadius;
        mStartCircleRadius = circleRadius;

    }


    public void setIsAlarm(boolean alarm) {
        this.isAlarm = alarm;
        postInvalidate();
        startAlarmAnimation(200);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mViewWidth = w;
        mViewHeight = h;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        mRingRadius = mBigCircleRadius / 6;
        mRingPaint.setStrokeWidth(mRingRadius);
        // 画圆和圆环
        canvas.drawCircle(mViewWidth / 2, (mViewHeight - tempRadius) / 2, mBigCircleRadius, mCirclePaint);
        canvas.drawArc(mViewWidth / 2 - mBigCircleRadius - mRingRadius / 2, (mViewHeight - tempRadius) / 2 - mBigCircleRadius - mRingRadius / 2, mViewWidth / 2 + mBigCircleRadius + mRingRadius / 2, (mViewHeight - tempRadius) / 2 + mBigCircleRadius + mRingRadius / 2, 180, mRingSweepAngle, false, mRingPaint);


        // 是否绘制中间的图片
//        if (mCenterImgIsVisible){
//            if (!isAlarm){
        mTempCenterImg = Bitmap.createScaledBitmap(mCenterImg, (int) mCenterImgWidth, (int) mCenterImgHeight, true);
        canvas.drawBitmap(mTempCenterImg, (mViewWidth / 2 - mTempCenterImg.getWidth() / 2), (mViewHeight - tempRadius) / 2 - mTempCenterImg.getHeight() / 2, null);
//            }else {
//                // alarm状态显示的图片
//                mTempAlarmTop = Bitmap.createScaledBitmap(mAlarmTop,(int)mAlarmTopWidth,(int)mAlarmTopHeight,true);
//                mTempAlarmBottom = Bitmap.createScaledBitmap(mAlarmBottom,(int)mAlarmBottomWidth,(int)mAlarmBottomHeight,true);
//
//                Paint paint = new Paint();
//                paint.setAlpha(mAlpha); //设置透明程度（alarm上面闪闪闪的几个点）
//
//                canvas.drawBitmap(mTempAlarmTop,(mViewWidth/2-mTempAlarmTop.getWidth()/2),(mViewHeight-tempRadius)/2-mTempAlarmTop.getHeight(),paint);
//                canvas.drawBitmap(mTempAlarmBottom,(mViewWidth/2-mTempAlarmBottom.getWidth()/2),(mViewHeight-tempRadius)/2-mTempAlarmTop.getHeight()/2,null);
//
//
//            }
//
//        }

        // 圆圈中间显示的倒计时的文字
        if (mCenterTextIsVisible) {
            mTextPaint.setTextSize(mTextSize);
            //计算baseline
            Paint.FontMetrics fontMetrics = mTextPaint.getFontMetrics();
            float distance = (fontMetrics.bottom - fontMetrics.top) / 2 - fontMetrics.bottom;
            float baseline = (mViewHeight - tempRadius) / 2 + distance;

            canvas.drawText("" + mCenterText, mViewWidth / 2, baseline, mTextPaint);
        }
    }

    /**
     * 动态改变圆的大小和位置
     *
     * @param change
     */
    public void changeCircle(float change) {
        // 圆中间改变图片的大小
        mCenterImgWidth = mStartCenterImgWidth - mStartCenterImgWidth / 2 * (change * 0.5f);
        mCenterImgHeight = mStartCenterImgHeight - mStartCenterImgHeight / 2 * (change * 0.5f);

        // 中间alarm状态图片大小随屏幕滑动改变
//        mAlarmTopWidth = mStartAlarmTopWidth-mStartAlarmTopWidth/2*(change*0.75f);
//        mAlarmTopHeight = mStartAlarmTopHeight-mStartAlarmTopHeight/2*(change*0.75f);
//        mAlarmBottomWidth = mStartAlarmBottomWidth-mStartAlarmBottomWidth/2*(change*0.75f);
//        mAlarmBottomHeight = mStartAlarmBottomHeight-mStartAlarmBottomHeight/2*(change*0.75f);

        // 改变原环和圆半径的大小
        tempRadius = (mViewHeight / 2 - mViewHeight / 4 + mBigCircleRadius / 2 + mRingRadius * 5) * change;
        mBigCircleRadius = mStartCircleRadius - ((mStartCircleRadius + mRingRadius * 2 + mRingRadius / 2) * (change * 0.5f));
        // 改变圆环的角度
        mRingSweepAngle = 180 + 180 * change;
        mTextSize = 135 - 135 / 2 * (change * 0.6f);
        postInvalidate();
    }

    public void startRingAnimation(long duration) {
        if (mRingAnimator != null && mRingAnimator.isRunning()) {
            mRingAnimator.cancel();
            mRingAnimator = null;
        }
        mRingAnimator = ValueAnimator.ofFloat(mRingSweepAngle, 360);
        mRingAnimator.setDuration(duration);
        mRingAnimator.setRepeatCount(0);
        mRingAnimator.setInterpolator(new LinearInterpolator());
        mRingAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                mRingSweepAngle = (float) animation.getAnimatedValue();
                postInvalidate();
            }
        });
        mRingAnimator.start();
    }

    public void startAlarmAnimation(long duration) {
        if (mAlarmAnimator != null && mAlarmAnimator.isRunning()) {
            mAlarmAnimator.cancel();
            mAlarmAnimator = null;
        }
        mAlarmAnimator = ValueAnimator.ofInt(1, 255);
        mAlarmAnimator.setDuration(duration);
        mAlarmAnimator.setRepeatCount(ValueAnimator.INFINITE);
        mAlarmAnimator.setInterpolator(new LinearInterpolator());
        mAlarmAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                mAlpha = (int) animation.getAnimatedValue();
                postInvalidate();
            }
        });
        mAlarmAnimator.start();
    }

    public void endRingAnimation() {
        mRingAnimator.reverse();
    }

}
