package com.yoyo.topscrollview;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.yoyo.topscrollview.centerview.CircleView;
import com.yoyo.topscrollview.centerview.WaveView;
import com.yoyo.topscrollview.common.CommonAdapter;
import com.yoyo.topscrollview.common.CommonUtils;
import com.yoyo.topscrollview.common.CommonViewHolder;
import com.yoyo.topscrollview.scrollview.ScrollParentView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {


    @BindView(R.id.scrollParentView)
    ScrollParentView mScrollParentView;
    @BindView(R.id.waveView)
    WaveView mWaveView;
    @BindView(R.id.image_topView)
    ImageView mImageTopView;
    @BindView(R.id.rl_transparentTop)
    RelativeLayout mTransparentTop;
    @BindView(R.id.circleView)
    CircleView mCircleView;
    @BindView(R.id.center)
    RelativeLayout mCenter;
    @BindView(R.id.ll_content)
    LinearLayout mLlContent;

    List<String> strings = new ArrayList<>();
    private CommonAdapter mTest1Adapter, mTest2Adapter, mTest3Adapter, mTest4Adapter;
    // 底部布局距顶部的距离
    float topHeight = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        initViewSize();
        initTestData();
        initAdapter();
        initBottomView();


        mScrollParentView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
            @Override
            public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                float v1 = scrollY / topHeight;
                if (0 <= v1 && v1 <= 1.1) {
                    mWaveView.changeWave(v1);
                    mCircleView.changeCircle(v1);
                }
            }
        });


    }

    private void initViewSize() {
        // 中间圆的半径
        mCircleView.setCircleRadius(CommonUtils.getWindowWidth(this) / 8);


        // 设置顶部图片的高度
        ViewGroup.LayoutParams params = mImageTopView.getLayoutParams();
        params.height = CommonUtils.getWindowHeight(this) / 3 + 100;
        mImageTopView.setLayoutParams(params);

        // 设置顶部透明部分的高度
        LinearLayout.LayoutParams layoutParams1 = (LinearLayout.LayoutParams) mTransparentTop.getLayoutParams();
        layoutParams1.height = CommonUtils.getWindowHeight(this) / 4;
        mTransparentTop.setLayoutParams(layoutParams1);

        //动态设置mCenterContent的高度
        LinearLayout.LayoutParams layoutParams2 = (LinearLayout.LayoutParams) mCenter.getLayoutParams();
        layoutParams2.height = CommonUtils.getWindowWidth(this) / 8 * 2 + CommonUtils.getWindowWidth(this) / 8 / 6 * 2;
        mCenter.setLayoutParams(layoutParams2);

        topHeight = CommonUtils.getWindowHeight(this) / 4;
    }

    /**
     * 动态添加底部布局
     */
    private void initBottomView() {
        int[] type = {1, 2, 3, 4};
        for (int i = 0; i < type.length; i++) {
            switch (type[i]) {
                case 1:
                    HomeBottomView test1View = new HomeBottomView(this, null);
                    test1View.setItemTitle("test 1");
                    test1View.setItemView(mTest1Adapter);
                    test1View.setItemTab(new String[]{"AAA", "BBB", "CCC", "DDD"});

                    mLlContent.addView(test1View);
                    break;
                case 2:
                    HomeBottomView test2View = new HomeBottomView(this, null);
                    test2View.setItemTitle("test 2");
                    test2View.setItemView(mTest2Adapter);
                    test2View.setItemTab(new String[]{"EEE"});

                    mLlContent.addView(test2View);
                    break;
                case 3:
                    HomeBottomView test3View = new HomeBottomView(this, null);
                    test3View.setItemTitle("test 3");
                    test3View.setItemView(mTest3Adapter);
                    test3View.setItemTab(new String[]{"FFF", "GGG"});


                    mLlContent.addView(test3View);
                    break;
                case 4:
                    HomeBottomView test4View = new HomeBottomView(this, null);
                    test4View.setItemTitle("test 4");
                    test4View.setItemView(mTest4Adapter);
                    test4View.setItemTab(new String[]{"HHH", "JJJ", "KKK"});


                    mLlContent.addView(test4View);
                    break;
            }
        }
    }

    /**
     * 底部列表里添加的一些测试数据
     */
    private void initTestData() {
        for (int i = 0; i < 6; i++) {
            strings.add("This is Test" + i);
        }
    }

    private void initAdapter() {
        mTest1Adapter = new CommonAdapter(strings, R.layout.layout_item) {
            @Override
            public void bindData(CommonViewHolder holder, int position, Object item) {
                holder.setText(R.id.tv_item, strings.get(position));
            }
        };
        mTest2Adapter = new CommonAdapter(strings, R.layout.layout_item) {
            @Override
            public void bindData(CommonViewHolder holder, int position, Object item) {
                holder.setText(R.id.tv_item, strings.get(position));
            }
        };
        mTest3Adapter = new CommonAdapter(strings, R.layout.layout_item) {
            @Override
            public void bindData(CommonViewHolder holder, int position, Object item) {
                holder.setText(R.id.tv_item, strings.get(position));
            }
        };
        mTest4Adapter = new CommonAdapter(strings, R.layout.layout_item) {
            @Override
            public void bindData(CommonViewHolder holder, int position, Object item) {
                holder.setText(R.id.tv_item, strings.get(position));
            }
        };
    }

}
