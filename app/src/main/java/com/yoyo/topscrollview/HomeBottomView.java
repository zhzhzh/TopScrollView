package com.yoyo.topscrollview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.tabs.TabLayout;
import com.yoyo.topscrollview.common.CommonAdapter;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * author:yoyo
 * date:2020/6/10
 */
public class HomeBottomView extends LinearLayout {

    @BindView(R.id.tv_homeItemtitle)
    TextView mHomeItemTitle;
    @BindView(R.id.tablayout_homeItemTab)
    TabLayout mHomeItemTab;
    @BindView(R.id.rv_homeItemView)
    RecyclerView mHomeItemView;

    public HomeBottomView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        View view = LayoutInflater.from(context).inflate(R.layout.layout_home_bottom_view_item, this);
        ButterKnife.bind(this, view);
    }

    /**
     * 设置标题名称
     *
     * @param title
     */
    public void setItemTitle(int title) {
        mHomeItemTitle.setText(title);
    }

    /**
     * 设置标题名称
     *
     * @param title
     */
    public void setItemTitle(String title) {
        mHomeItemTitle.setText(title);
    }

    /**
     * 设置类别
     *
     * @param type
     */
    public void setItemTab(String[] type) {
        for (int i = 0; i < type.length; i++) {
            mHomeItemTab.addTab(mHomeItemTab.newTab());
            mHomeItemTab.getTabAt(i).setText(type[i]);
        }
    }

    public void setItemView(CommonAdapter adapter) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);

        mHomeItemView.setAdapter(adapter);
        mHomeItemView.setLayoutManager(linearLayoutManager);
        mHomeItemView.setNestedScrollingEnabled(false);
    }


}